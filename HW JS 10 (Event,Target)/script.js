
// калькулятор
/*
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/



// калькулятор
let a, b, operator, d, sum, c, mrc;
let display = document.querySelector('.display')
let i = 0;
let reg2 = /[+-/*]/   //регулярка (оператори дІй)
// let reg = /\d/   //регулярка (цифра)

//це функція, яка працюватиме після кліку по =
let result =()=>{ 
 switch(operator){
    case '+':
    sum = parseFloat(a)+parseFloat(b)
    document.querySelector('.input_display').value= sum;
    break;
    case '-':
     sum = parseFloat(a)-parseFloat(b)
    document.querySelector('.input_display').value= sum;
    break;
    case '/':
     sum = parseFloat(a)/parseFloat(b)
    document.querySelector('.input_display').value= sum;
    break;
    case '*':
     sum = parseFloat(a)*parseFloat(b)
    document.querySelector('.input_display').value= sum;
    break;
    }
    a = sum;
    b = null;
    operator = null;
    return sum;
}
// функція для орбахунку, якщо замість = користувач натис якусь дію типу + - * /
let nextResult =()=> { 
    a = `${result()}`;
    }

// функція для орбахунку m-
let minusM = () =>{
    if (c === undefined || c === null){
        c = -parseFloat(document.querySelector('.input_display').value)
        return c}
    else {
        return c + (-parseFloat(document.querySelector('.input_display').value))
    }
}
// функція для орбахунку m+
let plusM = () => {
    if (c === undefined || c === null){
        c = parseFloat(document.querySelector('.input_display').value)
        return c}
    else {
        return c + (parseFloat(document.querySelector('.input_display').value))
    }
}


// робота при натисканні на mrc
 function mRc (){
    
    if (i===1 ){
    document.querySelector('.input_display').value = mrc; 
    document.getElementById('pm').remove();
    } 
    else{
    mrc = null;
    c= null;
    d = null;
    sum = null;
    a = null;
    b = null;
    operator = null;
    i=0;
    document.querySelector('.input_display').value = '0';

    }
    
}

//сам Калькулятор - Основна функція
document.querySelector('.keys').addEventListener('click', (e)=>{
    
    //1ше число
    if (e.target.closest('.button.black')  && ((a == null || a ==="") && operator == null) || (a == undefined && operator == undefined)){ //пишу перевірку a == null - щоб якщо число має бути 2-х значне тут в а не було перезапису
     a = e.target.value;
    document.querySelector('.input_display').value = e.target.value; 
    }
    else if(e.target.closest('.button.black')   && a !== null && (operator == null || operator == undefined)){ //це етап коли ми робимо 2хзначне число, буде перезапис а
    a = a + e.target.value;
    document.querySelector('.input_display').value = a; 
    }
    // else if (document.querySelector('.input_display').value === a && e.target.closest('dot')){

    // }


    
    //тут дії, якщо ввели оператор для розрахунку
    if (e.target == e.target.closest('.button.pink') && reg2.test(e.target.value) &&  (operator == null || operator == undefined)){
    operator = e.target.value; //записуэмо дію
    }
    //додатковий крок, якщо замість = коистувач нажимає наступну дію: + - / чи *
    else if(e.target == e.target.closest('.button.pink') && reg2.test(e.target.value) &&  (operator !== null || operator !== undefined)){
        nextResult(); // перезаписуємо а: в а записуємо результат попередньої дії
        operator = e.target.value; //після перезапису а, перезаписуємо оператор
        b = null; // обнуляємо b, щоб записати в нього наступну цифру для наступної дії, бо а і оператора вже перезаписали
    }

    //запис 2го числа

    if (e.target.closest('.button.black') && ((typeof(a) === typeof('a') || typeof(a) === typeof(1))&& typeof(operator) === typeof('operator')) && (b == null || b == undefined)){ //пишу перевірку a == null - щоб якщо число має бути 2-х значне тут в а не було перезапису
    b = e.target.value;
    document.querySelector('.input_display').value = e.target.value; 
    }
    else if(e.target.closest('.button.black') && typeof(a) === typeof('a') && typeof(b) === typeof('b') && typeof(operator) === typeof('operator')){ //це етап коли ми робимо 2хзначне число, буде перезапис а
    b = b + e.target.value;
    document.querySelector('.input_display').value = b; 
    }

    //тут якщо вже э2ге число, значить вже ввели дію, отже відкриваємо =
    if (typeof(a) === typeof('a') && typeof(b) === typeof('b') && typeof(operator) === typeof('operator')){
    document.querySelector('.button.orange').removeAttribute('disabled') //робимо активним =
    document.querySelector('.button.orange').addEventListener('click', result)
    }
    // обнулення при натисканны на С, але mrc - не обнуляється
    if(e.target.closest('#C')){
    sum = null;
    a = null;
    b = null;
    operator = null;
    
    document.querySelector('.input_display').value = '0';

    }


    //клік по m чи mrc  - запускаэмо функція, яка має шукати число яке ми ввели перед m-і зберігати його впам"яті як -число
    if(e.target.closest('#minusM') ){
        display.insertAdjacentHTML ('afterbegin', "<p id='pm'>m</p>")
    mrc = minusM(); 
    
    }
    if(e.target.closest('#plusM') ){
        display.insertAdjacentHTML ('afterbegin', "<p id='pm'>m</p>")
    mrc = plusM(); 
    
    }

    if(e.target.closest('#mrc')){
    i++
    mRc ();
    
  }

})




    




//слайдер
let box = document.querySelector('.box')
let ar = ["https://www.volynnews.com/files/news/2013/01-12/37435-5u.jpg","https://www.5.ua/media/pictures/400x266/205549.jpg?t=1610130918",
    "https://naurok.com.ua/uploads/blog/6%20%282%29.jpg","https://www.volynnews.com/files/news/2013/01-12/37435-22u.jpg"]

box.insertAdjacentHTML ( 'afterend', `<div class="block"><img class="slider" src="${ar[0]}" /></div>`)

setInterval(()=>{
    setTimeout(()=>{
    document.querySelector('.slider').src = ar[1]
    },3000);
    setTimeout(()=>{
    document.querySelector('.slider').src = ar[2]
    },6000);
    setTimeout(()=>{
    document.querySelector('.slider').src = ar[3]
    },9000);
    setTimeout(()=>{
    document.querySelector('.slider').src = ar[0]
    },12000)},

3000)