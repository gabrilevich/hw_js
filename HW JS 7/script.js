// Написати функцію `filterBy()`, яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних. - Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null]. 

//Завдання №1

function filterBy(t,str){
	for (var i in t){
		if (typeof(t[i]) === typeof(str)){
			continue;
		}
		else{
			 t_new.push(t[i]);
			} 
	console.log(`У циклі for ${t_new}`);
    }
    return t_new;
}
var t_new = [];//чи обов"язково оголошувати що тут масив? пробувала по-ішому, без вказання типу, чомусь не спрацьовувало, але може помилка ьула в іншому
const t = ['hello', 23, null,58, NaN,-478, 'Horse', [1,-56,'joke']];
const str = 'Horse'; //як можна просто вказати тип зімної, щоб не вписувати текст чи число
console.log(filterBy(t_new, t,str));


//Задача №2 Переписати гру шибениця з книги на новий синтасис.
let words = [
	"программа",
	"макака",
	"прекрасный",
	"оладушек"
	];
// Выбираем случайное слово
let word = words[Math.floor(Math.random() * words.length)];
// Создаем итоговый массив
let answerArray = [];
for (let i = 0; i < word.length; i++) {
	answerArray[i] = "_";
	}
let remainingLetters = word.length;
// Игровой цикл
while (remainingLetters > 0) {
// Показываем состояние игры
	alert(answerArray.join(" "));
	// Запрашиваем вариант ответа
	let guess = prompt("Угадайте букву, или нажмите Отмена для выхода из игры.");
	if (guess === null) {
		// Выходим из игрового цикла
		break;
		} else if (guess.length !== 1) {
		alert("Пожалуйста, введите одиночную букву.");
		} else {
		// Обновляем состояние игры
			for (let j = 0; j < word.length; j++) {
			if (word[j] === guess) {
			answerArray[j] = guess;
			remainingLetters--;
			}
		}
	}
// Конец игрового цикла
}
// Отображаем ответ и поздравляем игрока
alert(answerArray.join(" "));
alert(`Отлично! Было загадано слово ${word}`);