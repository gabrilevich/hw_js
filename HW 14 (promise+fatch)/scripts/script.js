import {req, show, showStars} from "./request.js"
//приклад; setTimeout для того, щоб зрегулювати в Прикладі саме показ відпрацювання помилки та коректного спрацювання. В 
// let promise = new Promise ((resolve, reject)=> //new Promise - це виклик /створення проміса. Проміс приймає ці 2параметри (так їх завжди пишемо: resolve - сюди пищемо, що робити, якщо все успішно; reject - що робити, якщо помилка ) Ця вся константа let promise - це лиш оголошення проміса і запуск його виконання
// {
// 	setTimeout(() => {
// 		resolve("Hello, my dear")},3000);
// 	setTimeout (() =>{
// 		reject(new Error("Error now"))},1000)
// })

// promise.then ( //метод .then - це якраз реалізація проміса на сторнці
// 	result => (alert(result)), //в першому виразі Завжди описуємо, що робити, якщо все Успішно
// 	error =>(alert(error)) //якщо є 2й вираз - то він вважається командоб для Помилки
// )

//fetch
// const url = "https://jsonplaceholder.typicode.com/users" //задаємо в константі url за яким будемо робити запит
// const req = fetch(url); //запускаємо обробку/відправку запита через fetch
// //дальше нам потрібно оробити і вивести результат, тому дальше все як в promise через then
// req
// .then ((i)=>{
// 	return i.json(); //оскільки в текстовому варіанті ми отримуємо json, то його треба отразу розпарсити і отримати дані в вигляді масиву, для цього на елементі, який тягнемо використовуємо метод.json(); i - це наче змына в яку записали результат, може називатися будь-як
// })
// .then((i)=>{ //оскільки в попередньому then ми обробляли результат, то в цьому then ми виводимо той оброблений результат
// console.log (i)
// })






document.querySelector(".nbu").addEventListener("click", ()=>{
	req("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json")
	.then(
	(response) => {
		console.log  (response)
		show (response)
	},
	error => console.log (error)
	)


})

document.querySelector(".star").addEventListener("click", ()=>{
	req("https://swapi.dev/api/people/")
	.then(
	(response) => {
		console.log  (response)
		showStars (response)
	},
	error => console.log (error)
	)


})





