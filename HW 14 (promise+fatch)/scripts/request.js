function req (url){

	return new Promise ((resolve, reject) => {
		document.querySelector(".lds-ring").classList.add("active")

		let p = new XMLHttpRequest();
		p.open ('GET', url)
		p.onreadystatechange = function () {
			if (p.status === 200 && p.readyState === 4){
				
				resolve (JSON.parse (p.response))
			} 
		}
		p.send()
	
})
}


let table = document.createElement ("div")
table.classList.add ("table")



function show (resp){
if (document.querySelector(".table")){
	document.querySelector(".table").remove() //видаляємо таблицю, якщо вона вже була опублікована для Зоряних війн
	document.querySelector(".star").classList.remove("hover")
		document.body.appendChild(table) //піблікуємо шапку таблиці, під яку дальше в переборі масиву буде публікуватися таблиця з даними
		table.innerHTML = `<table>
		<thead>
			<tr>
				<th>Назва</th>
				<th>Позначення</th>
				<th>Курс</th>
				<th>Дата курсу</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
		</table>`
	resp.forEach ((elem) =>{
	document.querySelector("table").insertAdjacentHTML("beforeend",`<tr>
		<td>${elem.txt}</td>
		<td>${elem.cc}</td>
		<td>${elem.rate}</td>
		<td>${elem.exchangedate}</td>
		
		</tr>` )
	})

}else{
		document.body.appendChild(table) //піблікуємо шапку таблиці, під яку дальше в переборі масиву буде публікуватися таблиця з даними
		table.innerHTML = `<table>
		<thead>
			<tr>
				<th>Назва</th>
				<th>Позначення</th>
				<th>Курс</th>
				<th>Дата курсу</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
		</table>`
	resp.forEach ((elem) =>{
	document.querySelector("table").insertAdjacentHTML("beforeend",`<tr>
		<td>${elem.txt}</td>
		<td>${elem.cc}</td>
		<td>${elem.rate}</td>
		<td>${elem.exchangedate}</td>
		
		</tr>` )
	})
	document.querySelector(".nbu").classList.add("hover")
	}
	document.querySelector(".lds-ring").classList.remove("active")
}


//Зоряні війни

function showStars (resp){
	let people = resp.results
	if (document.querySelector(".table")){
	document.querySelector(".table").remove() //видаляємо таблицю, якщо вона вже була опублікована для НБУ
	document.querySelector(".nbu").classList.remove("hover") //видаляємо підсвітку НБУ
		document.body.appendChild(table) //піблікуємо шапку таблиці, під яку дальше в переборі масиву буде публікуватися таблиця з даними
		table.innerHTML = `<table>
		<thead>
			<tr>
				<th>Ім'я</th>
				<th>Ріст</th>
				<th>Колір волосся</th>
				<th>Стать</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
		</table>`
	people.forEach ((elem) =>{
	document.querySelector("table").insertAdjacentHTML("beforeend",`<tr>
		<td>${elem.name}</td>
		<td>${elem.height}</td>
		<td>${elem.hair_color}</td>
		<td>${elem.gender}</td>
		
		</tr>` )
	})

}else{
		document.body.appendChild(table) //піблікуємо шапку таблиці, під яку дальше в переборі масиву буде публікуватися таблиця з даними
		table.innerHTML = `<table>
		<thead>
			<tr>
				<th>Ім'я</th>
				<th>Ріст</th>
				<th>Колір волосся</th>
				<th>Стать</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
		</table>`
	people.forEach ((elem) =>{
	document.querySelector("table").insertAdjacentHTML("beforeend",`<tr>
		<td>${elem.name}</td>
		<td>${elem.height}</td>
		<td>${elem.hair_color}</td>
		<td>${elem.gender}</td>
		
		</tr>` )
	})
	document.querySelector(".star").classList.add("hover")
}
document.querySelector(".lds-ring").classList.remove("active")
}











export {req, show, showStars};