// Завдання №1  - Прив'яжіть усім інпутам наступну подію - втрата фокусу кожен інпут виводить своє value в параграф з id="test" focus blur input  
// Завдання №2 - Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів. Скільки символів має бути в інпуті, зазначається в атрибуті data-length. Якщо вбито правильну кількість, то межа інпуту стає зеленою, якщо неправильна – червоною.

const [...inputs] = document.querySelectorAll("input");
test = document.querySelector("#test");

inputs.forEach ((input) => { 
    input.addEventListener("blur", (e) =>{
       // input.classList.add("in");
        let i = e.target.value;
        let dataLength = parseFloat(e.target.getAttribute('data-length'))
        if(i.length < dataLength){
            input.classList.add("borderRed");
        }
        else{
            input.classList.add("borderG");
        }
        test.innerText = i
     })
})

// Завдання №4 - 4.- При завантаженні сторінки показати користувачеві поле введення (input) з написом Price. Це поле буде служити для введення числових значень
// - Поведінка поля має бути такою:
// - При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
// - Коли забрали фокус з поля - його значення зчитується, над полем створюється span, в якому має бути виведений текст:
// .
// Поруч із ним має бути кнопка з хрестиком (X). Значення всередині поля введення фарбується зеленим.
// - При натисканні на Х - span з текстом та кнопка X повинні бути видалені.
// - Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою,
// під полем виводити фразу - Please enter correct price. span зі значенням при цьому не створюється.
window.onload = function (){
    const divPrice = document.createElement('div')
    divPrice.classList.add('divPrice')
    document.body.appendChild(divPrice)
//публікуємо input
    divPrice.innerHTML = '<input type="text" id="price" placeholder = "Price"></input>'
    const price = document.querySelector('#price')
//вішаємо фокус
    price.addEventListener('focus', ()=>{
        price.classList.add('borderG')
    })
    
//вішаємо зняття фокусу
    price.addEventListener('blur', (e)=>{
        price.className = ""
        let i = parseFloat(e.target.value)
        if(i<0){
            price.classList.add('.borderRed')
            divPrice.insertAdjacentHTML('beforeend','<p id="error">Please enter correct price</p>')

        }
        else{
        divPrice.insertAdjacentHTML('afterbegin', `<div class="divBut"><span>${i}</span><button id="sub">X</button></div>`)
        price.classList.add('back')
        }
        
        document.querySelector('#sub').onclick = function(){
        document.querySelector(".divBut").remove()
        }
    })  

}
