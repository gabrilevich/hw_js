//Завдання № 1 - Секундомір

let time = 0;
let min = 0;
let hour = 0;
let flag = false;

let timer;
let timerMin;
let timerHour;

let count = () =>{ //тут проста функція, яка просто рахує, як в циклі і записує цифру у вказаний елмент
	if (time < 60){     //поки не дыйшло до 60 - збільшується
		
		get(".sec").innerHTML = time;
		time++;
		
	}else{             //коли дохоить до 0, треба щоб Секунди починалися заново з 1
		time = 0;
		get(".sec").innerHTML = time;
		time++;
		min++; //звідси починаємо пистаи Хвилини
		if (min < 60){    
			get(".min").innerHTML = min;
		}else{         
			min = 0;
			get(".min").innerHTML = min;
			hour++;		//звідси починаємо пистаи Години				
			get(".hour").innerHTML = hour;
			}
	}
}


let res = () => {
	//потрібно обнулити інакше при Скинадані і подальшому запуску почнеться з тих цифр на яких скидали
	time = 0; 
	min = 0; 
	hour = 0; 
	//потрібно показати нулі на табло
	get(".sec").innerHTML = '00'; 
	get(".min").innerHTML = '00';
	get(".hour").innerHTML = '00';

}
let get = clas =>document.querySelector(clas); //це для скорочення, щоб постійно не писати document.querySelector

get("#start").onclick = () => { //шукаэмо кнопку Старт ы вышаэмо на неъ клік. після кліку створюємо таймер, який запускає рахунок кожні 1000 мілісек.. Рахунок іде з функції count
	if(!flag){
		timer = setInterval(count, 1000)//1000
		flag = true;
	}
	get('.container-stopwatch').classList.add('green')
	get('.container-stopwatch').classList.remove('black')
	get('.container-stopwatch').classList.remove('silver')
	get('.container-stopwatch').classList.remove('red')
}
get("#stop").onclick = () => { //шукаэмо кнопку Стоп ы вышаэмо на неъ клік.
	clearInterval(timer)
	get('.container-stopwatch').classList.add('red')
	get('.container-stopwatch').classList.remove('black')
	get('.container-stopwatch').classList.remove('silver')
	get('.container-stopwatch').classList.remove('green')
	flag = false;
}
get("#reset").onclick = () => { //шукаэмо кнопку Ресерт(скинути) ы вышаэмо на неъ клік
	clearInterval(timer)
	get('.container-stopwatch').classList.add('silver')
	get('.container-stopwatch').classList.remove('red')
	get('.container-stopwatch').classList.remove('black')
	get('.container-stopwatch').classList.remove('green')
	res();//треба функцыя на очищення даних
	flag = false;
}




// //Завдання №2 - перевірка номеру телефону
//створення div для поля вводу
let form = document.createElement('div')
form.classList.add('form');
document.body.appendChild(form);
let p = document.createElement('p')
form.appendChild(p);
p.innerHTML = "Ваш телефон";

//створення input, навышування атрибутів
let input = document.createElement('input')
input.classList.add('bd');
input.setAttribute('type','tel');
input.setAttribute('placeholder','000-000-00-00');
input.setAttribute("name",'tel');
form.appendChild(input);


//створення кнопки
let but = document.createElement('input')
but.setAttribute('type','button')
but.setAttribute('value','Зберегти')
but.classList.add('but');
form.appendChild(but);

//створення кнопки після помилки
let butRet = document.createElement('input')
butRet.setAttribute('type','button')
butRet.setAttribute('value','Заново')
butRet.classList.add('butRet');

let reset = ()=>{ //заміщення кнопки
	form.appendChild(butRet);
	but.remove();
}
let bt = () => { //заміщення кнопки
	document.querySelector('.bd').value = null;
 p.style.color = '#000000';
 butRet.remove();
 form.appendChild(but);

}

//просто функція для перевірки+якщо помилка тут же пишемо подальші дії
let g = ()=>{
	let t = document.querySelector('.bd').value; //в зміну записуємо - шукаємо input вводу по класу і пишемо value, щоб отримати введенні дані
	let regular = /\d{3}-\d{3}-\d{2}-\d{2}/  //регулярка для перевырки
	if(regular.test(t)){ //перевірка з регуляркою, якщо true, то на картинку
		form.style.backgroundColor = '#65f0bb';
		setTimeout(() =>{
			document.location = 'https://i.pinimg.com/736x/2a/61/fa/2a61fa6b69bb1c4118ee69625f76bd14.jpg'
		}, 2000)
		
	}else{ //тут що буде, якщо помилка
		const x = Error('Неправильний формат');
		p.style.color = '#fb065e';
		p.innerHTML = x; //замішення тексту на сповіщення про помилку
		reset();
			document.querySelector('.butRet').onclick = () => { //вішаємо подію на кнопку,щоб при нажиманні відбувався код всередині
			p.innerHTML = "Ваш телефон";
			bt();
			}
		}
}

document.querySelector('.but').onclick = () => { //вішаємо подію на основну кнопку,щоб при нажиманні відбувався код всередині
	g();
}
