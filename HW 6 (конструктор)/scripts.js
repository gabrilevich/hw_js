
//Завдання №1 -  готово
function Woker (name,surname,rate,days) { //тут ми оголошуэмо про створення конструктора і вказуємо які будуть в ньому параметри
	this.name = name;
	this.surname = surname;
	this.rate =  rate;
	this.days = days;
	this.getSalary = function (){ //тут параметр з вираховуванням ЗП, пишемо return, щоб воно обчислилось ы дало результат
		return rate * days;
	}
}
const user = new Woker ('Alex','Lex',30,25); //№1 працівник зі своїми даними
const user1 = new Woker ('Kevin','Mex',50,17); //№2 працівник зі своїми даними
document.write(`Результат по завданню №1 - обраховуємо ЗП <br>`)
document.write(`Працівник ${user.name} ${user.surname} отримує в місяць - ${user.getSalary()} $ <br>`); //вивели №1 працівника
document.write(`Працівник ${user1.name} ${user1.surname} отримує в місяць - ${user1.getSalary()} $`); //вивели №2 працівника


//Завдання №2 - клас MyString -  готово

function MyString (str) {
	this.rever = function (){
		return this.reverse();
	}
}
var stri = 'комічна дія сарказм'; для всіх пунктів в Завданні 2
function ft (a){
	document.write (a.split('').reverse().join(''));
}
ft (stri);


//метод ucFirst() - готово (але взяла рішення з інтернету по регулярці)
function ucFirst (stri){
	let a = stri.charAt(0);
	let b = a.toUpperCase();
	document.write( b + stri.slice(1));
}
ucFirst (stri);

//метод ucWords - приймає рядок та робить заголовною першу літеру кожного слова цього рядка

stri = stri.toLowerCase().replace(/^[\u00C0-\u1FFF\u2C00-\uD7FF\w]|\s[\u00C0-\u1FFF\u2C00-\uD7FF\w]/g, function(letter) {
    return letter.toUpperCase();
		});
document.write(stri);



//Завдання №3 - клас Phone - готово

function Phone (number, model, weight) {
	this.number = number;
	this.model = model;
	this.weight = weight;
	this.receiveCall = function (){
		return (`Телефонує ${model}`)
	}
	this.getNumber = function (){
		return (`Номер ${number}`)
	}
}
const Alcatel = new Phone(33333333, 'Alcatel', '235 g');
const Simens = new Phone(55555555, 'Simens', '300 g');
const Motorola = new Phone(22222222, 'Motorola', '250 g');
document.write(`${Alcatel.receiveCall()}, ${Alcatel.getNumber()} <br> ${Simens.receiveCall()}, ${Simens.getNumber()} <br> ${Motorola.receiveCall()}, ${Motorola.getNumber()}`);




// Завдання №4 - Створити клас Car , Engine та Driver.
class Driver {
	constructor (name, surname, fatherName, driving){
		this.name = name;
		this.surname = surname;
		this.fatherName = fatherName;
		this.driving = driving;
		this.pib = function(){
			return (`ПІБ: ${surname} ${name} ${fatherName} <br> Стаж - ${driving} роки`)
		}
	}
}


class Engine{
	constructor (power, producer){
		this.power = power;
		this.producer = producer;
		this.pib = function(){
			return (`Виробник - ${producer}, Потужність авто - ${power}`)
		}
	}
}



class Car {
	constructor (brand, clas, weight){
		this.brand = brand;
		this.clas = clas;
		this.weight = weight;
		this.driver = driverUser.pib();
		this.motor = carEngine.pib();
		this.start = function (){
			document.write('Поїхали')
		}
		this.stop = function (){
			document.write("Зупиняємося")
		}
		this.turnRight = function (){
			document.write("Поворот праворуч")
		}
		this.turnLeft = function (){
			document.write("Поворот ліворуч")
		}
		this.toString = function (){
			document.write (`${this.driver} <br> ${this.motor} <br> Бренд - ${brand} <br> Клас - ${clas}`)

		}
	}
}

const driverUser = new Driver ('Антон','Лебіль','констянтинович', 23);
const carEngine = new Engine (1.5, 'Ford Motor Company');
const carInf = new Car ("Ford","C-clas","2000 kg");


class Lorry extends Car {
	constructor (capacity){
		super();
	this.capacity = capacity;
	}
}

const lor = new Lorry ('3500 тон');


class SportCar extends Car {
	constructor (limit){
		super();
	this.limit = limit;
	}
}
const sport = new SportCar (250);
document.write("Виводимо результат по 3м класам Driver, Engine, Car:" + carInf.toString() + "<br>"); 
document.write(`Lorry наслідує метод з Car ${lor.turnLeft()} і має тонажність: ${lor.capacity} <br>`);
document.write(`SportCar наслідує Car ${sport.start()} і має швидкість: ${sport.limit}`);






//Завдання №5 Створити клас Animal та розширюючі його класи Dog, Cat, Horse.+ похід до ветеринара
//Для Ветеринара - зробила дещо по-іншому. Хотіла щоб викликалася тварина і для неї виводилися дані, тому одну функцію помістила в іншу.

class Animal{
	constructor (food,noise,location) {
	this.food = food;
	this.location = location;
	this.noise = noise;
		}	
	makeNoise(){
		document.write(`Така тварина каже ${this.noise}`)};
	eat (){
		console.log (`Така тварина їсть ${this.food}`)};
	sleep (){
		console.log ('Така тварина спить')};

}

class Dog extends Animal {
	constructor (noise,food,type,location){
		super(food,noise,location);
		this.type = type;

	}
	makeNoise(){
		document.write(`${this.type} каже ${this.noise}`)};
	eat (){
		document.write(`${this.type} їсть ${this.food}`)};
}

class Cat extends Animal {
	constructor (noise,food,type,location){
		super(food,noise,location);
		this.type = type;

	}
	makeNoise(){
		document.write(`${this.type} каже ${this.noise}`)};
	eat (){
		document.write(`${this.type} їсть ${this.food}`)};
}

class Horse extends Animal {
	constructor (noise,food,type,location){
		super(food,noise,location);
		this.type = type;

	}
	makeNoise(){
		document.write(`${this.type} каже ${this.noise}`)};
	eat (){
		document.write(`${this.type} їсть ${this.food}`)};
}


class Veterinarian extends Animal{
	constructor (){
		super ();
	}
	
	main(){
		const anim = [dog,cat,horse]
		for(let i = 0; i < anim.length; i++){
			var a = anim[i];
			treatAnimal (a);
			function treatAnimal (a) {
			document.write(`Ветеринар у місті ${a.location}: на прийомі ${a.type}. Тварина з виду ${a.type} їсть ${a.food} та видає звуки "${a.noise}" <br>`)
			}
		}
	}

}



const vet = new Veterinarian();
const animal = new Animal();
const cat = new Cat('мяв',"мишку",'Cat','Луцьк');
const horse = new Horse('і-го-го',"траву",'Horse','Київ');
const dog = new Dog('гав',"м'ясо",'Dog','Житомир');
 // vet.main();
vet.main();
