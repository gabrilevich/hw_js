// import {clickInputSize, clickSauceAdd, clickToppingAdd} from "./functionEvent.js"
//Відобпразити ціну, зробити перетягування на корж (драп і дров), вивід інф. про начинку і дропери. Зробити валідацію даних в полях замовлення: якщо поле не пройшло валідацію, то його рамка має стати червоною.ПІБ вводити укр. мовою.

const pizza = {
    size : [
        {name : "small", price : 40},
        {name : "mid", price: 55},
        {name : "big", price: 70}
    ],
    topping: [
        {name : "moc1",   price: 50, productName: "Сир звичайний"},
        {name : "moc2",   price: 55, productName: "Сир фета"},
        {name : "moc3",   price: 40, productName: "Моцарелла"},
        {name : "telya",  price: 80, productName: "Телятина"},
        {name : "vetch1", price: 30, productName: "Помiдори"},
        {name : "vetch2", price: 35, productName: "Гриби"},
    ],
    sauce: [
        {name: "sauceClassic", price : 20, productName: "Кетчуп" },
        {name: "sauceBBQ", price : 22, productName: "BBQ"},
        {name: "sauceRikotta", price : 24, productName: "Рiкотта"}
    ]
}
const pizzaSelectUser = {
   size : "",
   topping : [],
   sauce : "",
   price : 0
}


function clickInputSize(e) {
    if(e.target.tagName === "INPUT"){
        userSlectTopping(e.target.value)
    }
}
 const clickToppingAdd = (e)=> {
    if(e.target.tagName === "IMG"){
        userSlectTopping(e.target.id)
        move(e.target)
        topers(e.target.productName); 
    }
}

 const clickSauceAdd = (e)=> {
    if(e.target.tagName === "IMG"){
        userSlectTopping(e.target.id)
        move(e.target)
        topers(e.target.productName); 
    }
}


function userSlectTopping(topping) {
    //size = "big"
    if ("smallmidbig".includes(topping)) {
        pizzaSelectUser.size = pizza.size.find((el) => {
            return el.name === topping
        })
    } else if ("moc1moc2moc3telyavetch1vetch2".includes(topping)) {
        pizzaSelectUser.topping.push(pizza.topping.find(el => el.name === topping))
    } else if ("sauceClassicsauceBBQsauceRikotta".includes(topping)) {
        pizzaSelectUser.sauce = pizza.sauce.find(el => el.name === topping)           
    }
    pizzaSelectUser.price = show(pizzaSelectUser);
    document.querySelector("#price").textContent = pizzaSelectUser.price    

}

function show(pizza) {
    let price = 0;
    
    if (pizza.sauce !== "") {
        price += pizza.sauce.price;
        
    }
    if(pizza.topping.length > 0){
        price += pizza.topping.reduce((a,b)=>{
            return a + b.price
        }, 0)
    }
    if(pizza.size !== ""){
        price += pizza.size.price;
    }
    console.log(price);
    return price;
    
}


//Для виводу топінгів
   function topers(id){
    let t 
    let spanX = document.createElement('span')
    spanX.setAttribute('class', 'closeIt')

    let span = document.createElement('span')
    span.setAttribute('class', 'spanTop')

    let spanP = document.createElement('span')
    spanP.setAttribute('class', 'spanP')
   
    //вивід  топінгів

if ("moc1moc2moc3telyavetch1vetch2".includes(id)) {
    t = pizza.topping.find(el => el.name == id).productName
    let parentDiv = document.querySelector('#topping')
    parentDiv.appendChild(spanP)
    spanP.append(span)
    spanP.append(spanX)
    spanX.innerHTML = '&times;'
    span.textContent = t}
    //вивід соусів
else if ("sauceClassicsauceBBQsauceRikotta".includes(id)) { 
    t =pizza.sauce.find(el => el.name == id).productName
    let parentDiv = document.querySelector('#sauce')
    parentDiv.appendChild(spanP)
    spanP.append(span)
    spanP.append(spanX)
    spanX.innerHTML = '&times;'
    span.textContent = t
    }  
}
           



//перенесення топінгів на піцу: Спосіб 2
function dragStart (e) {
    e.dataTransfer.effectAllowed = "move";
    e.dataTransfer.setData("Text", e.target.id);
    
}

function dragOver (e) {
    if (e.preventDefault) e.preventDefault();//відключаємо подію за замовчуваням в браузері
}

function drop (e) {
    if (e.preventDefault) e.preventDefault();
    if (e.stopPropagation) e.stopPropagation(); //зупиняємо поширення події на елементи нижче
    let id = e.dataTransfer.getData("Text"); 
    let elem = document.getElementById(id).cloneNode(true);  //клонуємо елемент, щоб візуально його кинути на піцу               
    this.appendChild(elem);
    userSlectTopping(id);
    topers(id)
    
       
}
document.querySelectorAll(".draggable").forEach((el) => {
    el.addEventListener('dragstart', dragStart)
}); 




//видалення топінгу


document.querySelector(".topings").addEventListener('click', (e)=>{ //відслідковуємо клік на Батьківському, тому, що він завжди є, ще до додавння топінгів на вивід
    //видалення запису в пунктах виводу назв Доданих топінгів і соусів.
    if(e.target.classList.contains("closeIt")){//пишемо перевірку, щоб бачити чи є той елемент в HTML на який ми клікаємо, тому що він додається динамічно
    let r = e.target.closest('.spanP'); //батьківський span має id.то ми вказуємо - видалити найближкий елемент з таким id при спрацьовуванні кліку
    r.remove()
    }
    
    //видалення картинки з коржа
   let topNameClick = e.target.closest('.spanP').textContent //витягаємо текст на Х біля якого натиснув корпистувач, щоб видалити
   let topNameRemove = topNameClick.substring(0, topNameClick.length - 1) //витягаємо назву топінгу - видаляємо Х біля неї, щоб в подальшому знайти чрез назву картинку на піці
    let u = pizza.topping.find(el => el.productName == topNameRemove).name //маючи назву топінгу, який =productName, йдемо в об'єкт Піци і шукаємо name, який выдповыдаэ нащому productName
    let parentTable = document.querySelector(".table")//шукаэмо корж, на якому картинка
    parentTable.querySelector(`#${u}`).remove() // шукаэмо картинку ы видаляэмо
    

    //видалення топінгу з pizzaSelectUser
    let ur = pizzaSelectUser.topping.find(el => el.productName == topNameRemove) //маючи назву топінгу, який =productName, йдемо в об'єкт pizzaSelectUser і шукаємо елемент повныстю, який выдповыдаэ нащому productName
    pizzaSelectUser.topping = pizzaSelectUser.topping.filter(el => el !== ur) //через filter відфільтровуємо масив із pizzaSelectUser.topping і filter видаляє вказаний елемент !== ur і перезаписуємо наш масив в pizzaSelectUser.topping
 
   
    //видаляэмо з ціни pizzaSelectUser вартість видаленого топінгу
    let uPrice = pizza.topping.find(el => el.productName == topNameRemove).price
    console.log(pizzaSelectUser.price + "ціна до видалення")
    pizzaSelectUser.price = pizzaSelectUser.price - uPrice
    console.log(pizzaSelectUser)

    
    //вивід нової ціни для користувача
    document.querySelector("#price").textContent = pizzaSelectUser.price //робимо зміни в виведенні загальної ціни
    console.log(pizzaSelectUser.price  + "ціна в обэкты")
} )

//видалення соусу

document.querySelector(".sauces").addEventListener('click', (e)=>{ //відслідковуємо клік на Батьківському, тому, що він завжди є, ще до додавння топінгів на вивід
    //видалення запису в пунктах виводу назв Доданих топінгів і соусів.
    if(e.target.classList.contains("closeIt")){//пишемо перевірку, щоб бачити чи є той елемент в HTML на який ми клікаємо, тому що він додається динамічно
    let r = e.target.closest('.spanP'); //батьківський span має id.то ми вказуємо - видалити найближкий елемент з таким id при спрацьовуванні кліку
    r.remove()
    }
    
    //видалення картинки з коржа
   let topNameClick = e.target.closest('.spanP').textContent //витягаємо текст на Х біля якого натиснув корпистувач, щоб видалити
   let topNameRemove = topNameClick.substring(0, topNameClick.length - 1) //витягаємо назву топінгу - видаляємо Х біля неї, щоб в подальшому знайти чрез назву картинку на піці
    let u = pizza.sauce.find(el => el.productName == topNameRemove).name //маючи назву топінгу, який =productName, йдемо в об'єкт Піци і шукаємо name, який выдповыдаэ нащому productName
    console.log(u)
    let parentTable = document.querySelector(".table")//шукаэмо корж, на якому картинка
    parentTable.querySelector(`#${u}`).remove() // шукаэмо картинку ы видаляэмо
    
    
    //видаляэмо з ціни pizzaSelectUser вартість видаленого топінгу
    let uPrice = pizza.sauce.find(el => el.productName == topNameRemove).price
    console.log(pizzaSelectUser.price + "ціна до видалення")
    pizzaSelectUser.price = pizzaSelectUser.price - uPrice
    console.log(pizzaSelectUser)

    //вивід нової ціни для користувача
    document.querySelector("#price").textContent = pizzaSelectUser.price //робимо зміни в виведенні загальної ціни
    console.log(pizzaSelectUser.price  + "ціна в обэкты")
} )




//запуск копіювання картинки топінга на корж піци
document.querySelector(".table").addEventListener("dragover", dragOver);

document.querySelector(".table").addEventListener("drop", drop);


//Прослуховування кліків (основні дії)
document.getElementById("pizza").addEventListener("click", clickInputSize);
   
document.querySelectorAll(".topping").forEach((div)=>{
   div.addEventListener("click", clickToppingAdd)
  
})

document.querySelectorAll(".sauce").forEach((div)=>{
   div.addEventListener("click", clickSauceAdd)
   
})
    


//Валідація даних у формі замовлення
document.querySelector("#nameBs").addEventListener('change', ()=>{
    let regular = /([А-яЇїЄєЙйґҐ])/
    let i = document.querySelector("#nameBs").value
    if(regular.test(i) == false){
        document.querySelector("#nameBs").classList.add('redBorder')

    }else{
        document.querySelector("#nameBs").classList.remove('redBorder')
    }

})
document.querySelector("#ph").addEventListener('change', ()=>{
    let regular = /\+\d{1,12}/
    let i = document.querySelector("#ph").value
    if(regular.test(i) == false){
        document.querySelector("#ph").classList.add('redBorder')

    }else{
        document.querySelector("#ph").classList.remove('redBorder')
    }

})
document.querySelector("#em").addEventListener('change', ()=>{
    let regular = /\w+@\w+\.\w+/
    let i = document.querySelector("#em").value
    if(regular.test(i) == false){
        document.querySelector("#em").classList.add('redBorder')

    }else{
        document.querySelector("#em").classList.remove('redBorder')
    }

})
//зміщення банера при наведені
document.querySelector("#banner").addEventListener("mouseover", (e) =>{
    let banner = document.querySelector("#banner")
    banner.classList.add("bannerMouve"); //додаємо клас з position: absolute , щоб елемент не був закріплений
    let bannerFind = e.target.getBoundingClientRect() //записуємо як об'єкт координати баннера, щоб потім їх змінювати при наведені миші
    banner.style.right = bannerFind.x + "px"    // змінюємо розміщення, щоб баннер зміщувався при наведені мишкою. В даному випадкe механізм такий: береться точка Х для баннера відносно вікна браузера і до неї додаються px, оскільки ми змінюємо стиль (відстань від правого боку екрану), то елемент зміщується на ту кількість px, що були в точці Х на початку для баннера. Потім, коли відбулося перше зміщення, банер має вже нову координату Х, адже він змістився і тепер йому знову дається px і принаведені відбувається зміна його відстані від правого кута, тому він знову зміщується
   
})



//перенесення топінгів на піцу мій спосіб
//     window.onload = function () {
//     let quer = w =>document.querySelector(w);
//     let move = quer('.ingridients').addEventListener('mousedown', (e)=>{

//             //робимо копыю картинки, щоб при клыковы створювалася ъъ копыя, а не змыщувалася основна картинка
//             var img  = document.createElement('img') //сторюэмо картинку
//             img.setAttribute('src', e.target.src) //передаэмо новый кратинці адресу самої кратинки, щоб візуально було видно рух картинки
            
//             //задаємо позиціонування при натиску мишкою, щоб елемент ставав поверх всього при натиску
//             img.style.position = 'absolute';
//             img.style.zIndex = 1000;
//             document.body.append(img); //ніби публікуємо наш елемент, щоб він був в рамках нашого вікна/документа, а не вилазив за його межі
//             //робимо функцію, щоб задати межі елемента
//             function moveAt(pageX, pageY) {
//            img.style.left = pageX - img.offsetWidth / 2 + 'px';
//            img.style.top = pageY - img.offsetHeight / 2 + 'px';
//           }
//           //робимо, щоб елемент знаходився на місці натиску (під курсором), а не десь на документі. pageX та pageY - передають браузеру інф. про знаходження курсору, далі фіксується через е подія за даними, де курсор, тобто події/кліку/ елементу ми кажемо, що будь тут де курсор, інф. про яку дали  pageX та pageY
//           moveAt(e.pageX, e.pageY); //передаємо це у функцію вище, щоб задати зміщення елементу, на який клікнули
         
//         function onMouseMove(e) { // функція, щоб елемент ставав під курсор
//             moveAt(e.pageX, e.pageY);
//           }

//         document.addEventListener('mousemove', onMouseMove) //тепер задаэмо подыю, щоб документ відкслідковував рух, адже ми опублікували елемент для пересування саме в document. Тепер при рухову курсора запускається функція onMouseMove, яка буде рухати еемент за курсором
        
//         //Спуск елемента - тепер робимо так, що коли ідпускаємо клавішу мишки елемент падав/зупинявся у тому місці, де курсор відпустили
//          img.onmouseup = function() {
//             document.removeEventListener('mousemove', onMouseMove); //видаляємо слухачі подій, щоб елемент впав, бо інакше буде дальше бігаи за курсором
//             quer('.ingridients').onmouseup = null;
//           };
          

//     })
//     //видаляємо обробник drag’n’drop у браузері, щоб він не дублював елемент
//         quer('.ingridients').ondragstart = function() {
//           return false;
//         };
// }