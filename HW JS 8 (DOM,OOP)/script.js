function p(){
	const diametr = Math.ceil(parseInt(prompt('Введіть однозначне число')));
	const input = document.querySelector('input')
	input.remove();

	const div = document.createElement('section');//створюємо основний div, щоб в ньому через flex розмістити інші кола-div
	div.style.cssText = `display:flex; flex-wrap: wrap; `//додаємо головному div властивості flex, щоб кола ставали в ряд
	div.style.width = diametr * 10 + "px"; // задаємо потрібну ширину секції, аби в ряд вміщались рівно 10 кіл
	document.body.appendChild(div);//публікуємо/додаємо цей div на сторінку в body


		for(let i = 0; i < 100; i++){ //цикл, щоб створити і вивести кола
			const circle = document.createElement('div'); //створюємо div, який будемо перетворювати в коло 
			// circleArr[i] = circle;
			circle.style.cssText=`height:${diametr}px; width:${diametr}px;  border-radius: 50%;`//задаємо базові стилі для div-коло, щоб воно стало колом
			circle.style.backgroundColor = `hsl(${Math.ceil(Math.random() * 360)}, 50%, 50%)`;// робимо різний колір для кожного кола через рандомне число Math.random() і його заокруглення Math.ceil
			div.appendChild(circle); // публікуємо div-коло в головному div
			
		}
		
	}
	document.querySelector('body').onclick = function(e) { //шукаємо div Батьківський і вішаємо на них onclick через один рядок
	  const btn = e.target.closest('div'); //closest - це найближчий, target- це елемент, е- подія, це все здаємо як зміну. тобто подія на елементі і шукаємо найближчий до нього ('div')
	  btn.remove(); //видаляємо найближчий div по якому був клік
	
	}

	



